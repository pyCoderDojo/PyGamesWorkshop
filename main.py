import pygame, os, sys
from pygame.locals import *
from PIL import Image

pygame.init()
fpsClock = pygame.time.Clock()
surface_x = 1800
surface_y = 480
surface = pygame.display.set_mode((surface_x, surface_y))
background = pygame.Color(100, 149, 237)
image = pygame.image.load('canvas.png').convert_alpha()
sun = pygame.image.load('SunRed.png').convert_alpha()
sun_mask = pygame.mask.from_surface(sun)
print('sun_mask: ', sun_mask)
sun_rect = sun.get_rect(center = surface.get_rect().center)
print('sun_rect: ', sun_rect)
meeple = pygame.image.load('meeple1.png').convert_alpha()
moving_meeple = pygame.mask.from_surface(meeple)
meeple_exit = pygame.image.load('explode_meeple.png').convert_alpha()
moving_meeple_exit = pygame.mask.from_surface(meeple_exit)
meeple_x = 100
meeple_y = 100
sun_x = 250
sun_y = 250



with Image.open('explode_meeple.png') as im:
    print('getbbox: ', im.getbbox())
    print('getchannel: ', im.getchannel('A'))
    print('getpixel: ', im.size)

with Image.open('meeple1.png') as meeple_png:
    meeple_size_x, meeple_size_y = meeple_png.size[0], meeple_png.size[1]

alive = True

while True:
    surface.fill(background)
    surface.blit(image, (0,0))
    surface.blit(sun, (sun_x, sun_y))
    surface.blit(meeple, (meeple_x, meeple_y)) if alive else surface.blit(meeple_exit, (meeple_x, meeple_y))

    pygame.display.update()
    fpsClock.tick(30)

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            print(meeple_x, '/', meeple_y)
            step_size = 50
            if event.key == pygame.K_RIGHT and meeple_x < (surface_x - 1.25 * meeple_size_x):
                meeple_x += step_size
            if event.key == pygame.K_LEFT and meeple_x > (meeple_size_x - 0.5 * meeple_size_x):
                meeple_x -= step_size
            if event.key == pygame.K_DOWN and meeple_y < (surface_y - 1.5 * meeple_size_y):
                meeple_y += step_size
            if event.key == pygame.K_UP and meeple_y > (meeple_size_y - 0.5 * meeple_size_y):
                meeple_y -= step_size

        offset = (meeple_x - sun_x), (meeple_y - sun_y)
        if moving_meeple.overlap(sun_mask, offset):
            print("sudden death")
            alive = False
